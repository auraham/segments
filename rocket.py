class DefaultColor(object):
    """
    This class should have the default colors for every segment.
    Please test every new segment with this theme first.
    """
    # RESET is not a real color code. It is used as in indicator
    # within the code that any foreground / background color should
    # be cleared
    RESET = -1

    USERNAME_FG = 162
    USERNAME_BG = 232
    USERNAME_ROOT_BG = 124

    HOSTNAME_FG = 39
    HOSTNAME_BG = 232

    HOME_SPECIAL_DISPLAY = True
    HOME_BG = 31  # blueish
    HOME_FG = 15  # white
    PATH_BG = 232  # dark grey
    PATH_FG = 250  # light grey
    CWD_FG = 67
    SEPARATOR_FG = 244

    READONLY_BG = 124
    READONLY_FG = 254

    SSH_BG = 166  # medium orange
    SSH_FG = 254

    REPO_CLEAN_BG = 232
    REPO_CLEAN_FG = 35
    REPO_DIRTY_BG = 232
    REPO_DIRTY_FG = 162

    JOBS_FG = 39
    JOBS_BG = 238

    CMD_PASSED_BG = 232
    CMD_PASSED_FG = 245
    CMD_FAILED_BG = 232
    CMD_FAILED_FG = 197

    SVN_CHANGES_BG = 148
    SVN_CHANGES_FG = 22  # dark green

    GIT_AHEAD_BG = 232
    GIT_AHEAD_FG = 61
    GIT_BEHIND_BG = 240
    GIT_BEHIND_FG = 250
    GIT_STAGED_BG = 232
    GIT_STAGED_FG = 2
    GIT_NOTSTAGED_BG = 232
    GIT_NOTSTAGED_FG = 30
    GIT_UNTRACKED_BG = 232
    GIT_UNTRACKED_FG = 244
    GIT_CONFLICTED_BG = 9
    GIT_CONFLICTED_FG = 15

    GIT_STASH_BG = 221
    GIT_STASH_FG = 0

    VIRTUAL_ENV_BG = 232  # a mid-tone green
    VIRTUAL_ENV_FG = 35

    BATTERY_NORMAL_BG = 22
    BATTERY_NORMAL_FG = 7
    BATTERY_LOW_BG = 196
    BATTERY_LOW_FG = 7

    AWS_PROFILE_FG = 39
    AWS_PROFILE_BG = 238

    TIME_FG = 8
    TIME_BG = 232


class Color(DefaultColor):
    """
    This subclass is required when the user chooses to use 'default' theme.
    Because the segments require a 'Color' class for every theme.
    """
    pass
