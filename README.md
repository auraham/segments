# Segments

This repo contains custom segments for `powerline-shell` ([source](https://github.com/b-ryan/powerline-shell)).

Download this repo:

```
$ cd ~/git
$ git clone git@gitlab.com:auraham/segments.git
```

and copy `config.json` in your home directory:

```
$ cd ~/.config/powerline-shell
$ cp ~/git/segments/config.json .
```

